﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRoad : MonoBehaviour
{
    int laneNumber = 1,
        lanesCount = 2;
    public float FirstLanePos,
                 LaneDistance,
                 SideSpeed;
    void Update()
    {
        CheckInput();
        Vector3 newpos = transform.position;
        newpos.z = Mathf.Lerp(newpos.z, FirstLanePos + (laneNumber * LaneDistance), Time.deltaTime * SideSpeed);
        transform.position = newpos;

    }
    void CheckInput()
    {
        int sign = 0;

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            sign = -1;
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            sign = 1;
        else
            return;
        laneNumber += sign;
        laneNumber = Mathf.Clamp(laneNumber, 0, lanesCount);
    }
}
