﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour
{
    public WheelCollider[] wc;
    public GameObject[] wheels;
    public float torque = 200;
    void Start()
    {

    }

    void Go(float accel)
    {

        accel = Mathf.Clamp(accel, -1, 1);
        float trustTorque = accel * torque;
        

        for (int i = 0; i < 4; i++)
        {
            wc[i].motorTorque = trustTorque;
            Quaternion quat;
            Vector3 position;
            wc[i].GetWorldPose(out position, out quat);
            wheels[i].transform.position = position;
            wheels[i].transform.rotation = quat;
        }
        
    }

    void Update()
    {
        float a = Input.GetAxis("Vertical");
        Go(a);
    }
}
