﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject target;
    public float smoothTime = 0.3f;
    public float xOffset;

    Vector3 velocity = Vector3.zero;
 
    void Update()
    {
        Vector3 targetPos = new Vector3(target.transform.position.x + 15, target.transform.position.y + 10f, transform.position.z);
        /*if (targetPos.y < transform.position.y)
            return;*/

        /*targetPos = new Vector3(targetPos.x, targetPos.y,transform.position.z);*/
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
        transform.position = targetPos;
    }
}
